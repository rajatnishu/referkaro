import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../service/apiService';
@Component({
  selector: 'app-otp-page',
  templateUrl: './otp-page.page.html',
  styleUrls: ['./otp-page.page.scss'],
})
export class OtpPagePage implements OnInit {

  constructor(private router: Router, private apiservice: ApiService) { }

  ngOnInit() {
    this.mobile = localStorage.getItem("mobile")
  }
  mobile: any
  number1: any
  number2: any
  number3: any
  number4: any

  login() {
    let obj = {
      mobile: this.mobile,
      otp: JSON.stringify(this.number1) + JSON.stringify(this.number2) + JSON.stringify(this.number3) + JSON.stringify(this.number4),
      deviceid: "hfghfjdh"
    }
    debugger
    this.apiservice.otpVerifyApi(obj).subscribe((res: any) => {
      if (res.status == 0) {
        this.apiservice.dengerMessage(res.error)
      } else {
        localStorage.setItem('token', res.access_token)
        debugger
        localStorage.setItem('id',res.data.id)
        this.apiservice.successMessage('Login succesfully!')
        this.router.navigate(['/', 'folder'])
      }
    }, error => {
      this.apiservice.dengerMessage(error.message)
    })
  }


  otpController(event,next,prev){
    if(event.target.value.length < 1 && prev){
      prev.setFocus()
    }
    else if(next && event.target.value.length>0){
      next.setFocus();
    
    }
    else {
      this.login()
     return 0;
     
    } 
 }

  
}

