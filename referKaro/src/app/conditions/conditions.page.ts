import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ApiService } from '../service/apiService';


@Component({
  selector: 'app-conditions',
  templateUrl: './conditions.page.html',
  styleUrls: ['./conditions.page.scss'],
})
export class ConditionsPage implements OnInit {

  constructor(private _location : Location, private api: ApiService) { }

  ngOnInit() {
    this.condition()
  }


  onBack() {
    this._location.back();
  }
data  :  any 
  condition(){
    this.api.getCondition().subscribe((res : any) =>{
      debugger
        this.data  =  res.data.page_text
    }, error =>{
      this.api.dengerMessage(error.message)
    })
  }
}
