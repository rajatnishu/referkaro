import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common"
import { ApiService } from '../service/apiService';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {

  constructor(  private api  :  ApiService,  private _location:Location,) { }

  ngOnInit() {
    this.getUser()
  }

  onBack() {
    this._location.back();
  //  this.router.navigate(['/', 'tab3'])
  }


  userData : any = { } 
  getUser(){
    let id = localStorage.getItem('id')
    this.api.getLoginUser(id).subscribe((res : any ) =>{
        this.userData = res.data
    }, error =>{
      this.api.dengerMessage(error.error)
    })
  }

}
