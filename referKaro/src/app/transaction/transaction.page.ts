import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service/apiService'

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.page.html',
  styleUrls: ['./transaction.page.scss'],
})
export class TransactionPage implements OnInit {

  constructor( private ApiService : ApiService,   private _location:Location) { }

  ngOnInit() {
    this.getAllTrasection()
  }

  onBack() {
    this._location.back();
  }

  listTrasection : any = []

  getAllTrasection(){
    this.ApiService.trasection().subscribe((res: any) => {
      this.listTrasection = res.data
      debugger
    }, error => {
      this.ApiService.dengerMessage(error.message)
    })
  }
}
