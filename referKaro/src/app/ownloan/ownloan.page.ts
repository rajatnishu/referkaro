import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/apiService';


@Component({
  selector: 'app-ownloan',
  templateUrl: './ownloan.page.html',
  styleUrls: ['./ownloan.page.scss'],
})
export class OwnloanPage implements OnInit {

  constructor(private ApiService: ApiService, private _location: Location) { }

  ngOnInit() {
    this.getOwnLoan()
  }

  onBack() {
    this._location.back();
  }

  listLoan  : any = []

  getOwnLoan() {
    this.ApiService.ownLoan().subscribe((res: any) => {
      this.listLoan = res.data
      debugger
    }, error => {
      this.ApiService.dengerMessage(error.message)
    })
  }

}
