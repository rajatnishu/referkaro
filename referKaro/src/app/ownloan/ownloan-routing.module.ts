import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OwnloanPage } from './ownloan.page';

const routes: Routes = [
  {
    path: '',
    component: OwnloanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OwnloanPageRoutingModule {}
