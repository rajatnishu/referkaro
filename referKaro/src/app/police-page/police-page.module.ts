import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PolicePagePageRoutingModule } from './police-page-routing.module';

import { PolicePagePage } from './police-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PolicePagePageRoutingModule
  ],
  declarations: [PolicePagePage]
})
export class PolicePagePageModule {}
