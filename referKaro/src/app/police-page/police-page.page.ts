import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ApiService } from '../service/apiService';

@Component({
  selector: 'app-police-page',
  templateUrl: './police-page.page.html',
  styleUrls: ['./police-page.page.scss'],
})
export class PolicePagePage implements OnInit {

  constructor(private _location  : Location, private api : ApiService) { }

  ngOnInit() {
    this.police()
  }


  onBack() {
    this._location.back();
  }

  data : any
  police(){
    this.api.police().subscribe((res : any) =>{
this.data = res.data.page_text
    }, error =>{
      this.api.dengerMessage(error.message)
    })
  }

}
