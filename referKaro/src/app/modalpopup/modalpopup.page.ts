import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'
import { ApiService } from '../service/apiService';

@Component({
  selector: 'app-modalpopup',
  templateUrl: './modalpopup.page.html',
  styleUrls: ['./modalpopup.page.scss'],
})
export class ModalpopupPage implements OnInit {
 
  // value;
  constructor(private ApiService: ApiService, private ModalController: ModalController) { }
  @Input() value: any;

  
  ngOnInit() {
    this.allService()
  }

  listService: any = []

  allService() {
    this.ApiService.allService().subscribe((res: any) => {
      this.listService = res.data
    }, error => {
      this.ApiService.dengerMessage(error.message)
    })
  }

  CloseModal() {
    this.ModalController.dismiss();
  }
  serviceId: any

  openModel() {
    if (this.serviceId) {
      let obj = {
        mobile: this.value,
        service_id: this.serviceId
      }
      this.ApiService.sendRefer(obj).subscribe((res: any) => {
        if (res.status === 1) {
          this.ApiService.successMessage(res.message)
          this.CloseModal()
        } else {
          this.ApiService.dengerMessage(res.message)
        }
      }, error => {
        this.ApiService.dengerMessage(error.message)
      })
    } else {
      this.ApiService.dengerMessage('first select service then click on button!')
    }
  }

  serviceName  : any
  selectService(data) {
    this.serviceName =  data.name
    this.serviceId = data.id
  }

}
