import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
// import { FCM } from '@ionic-native/fcm/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/folder/home', icon: 'home' },
    { title: 'Terms & Conditions', url: '/conditions', icon: 'document-text' },
    { title: 'Profile', url: '/profile', icon: 'person-circle' },
    { title: 'privacy policy', url: '/police-page', icon: 'lock' },
    { title: 'Logout', url: '/first-page', icon: 'log-out' }
  ];
  // public labels = <ion-icon name="person-outline">privacy policy <ion-icon name="person-circle-outline"></ion-icon></ion-icon> <ion-icon name="people-outline"></ion-icon> ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(public router: Router, private platform: Platform) {
    this.initializeApp();
  }

  user: any

  initializeApp() {
  
   
    this.platform.ready().then(() => {
      this.loadUIDisplay();      
      if (this.user) {
        this.router.navigateByUrl('/folder');
      }
      else {
        this.router.navigateByUrl('/first-page');
      }
    });
    
  }

  loadUIDisplay() {
    try {
      this.user = localStorage.getItem("token");
    } catch (err) {

    }
  }

  logOut(data) {
    if (data.title == 'Logout') {
      localStorage.clear()
      this.router.navigateByUrl('/first-page');
    }else{
      this.router.navigateByUrl(data.url)
    }
  }


}

