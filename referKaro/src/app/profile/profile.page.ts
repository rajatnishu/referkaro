import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ApiService } from '../service/apiService';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor( private _location:Location, private api : ApiService) { }

  ngOnInit() {
    this.getUser()
  }

  onBack() {
    this._location.back();
  }

  userData : any = { } 
  getUser(){
    let id = localStorage.getItem('id')
    this.api.getLoginUser(id).subscribe((res : any ) =>{
        this.userData = res.data
    }, error =>{
      this.api.dengerMessage(error.error)
    })
  }


  updateUserProfile : boolean = true  

  updateProfile(data){
    this.updateUserProfile =  data
  }

  profile(){
    debugger
    let obj ={
      email : this.userData.email,
      name :  this.userData.name,
      mobile  : this.userData.mobile
    }
    this.api.updateUser(obj).subscribe((res : any)=>{
      debugger
      this.updateUserProfile = true
      this.userData =  res.data
      this.api.successMessage(res.message)
    }, error =>{
      this.api.dengerMessage(error.error.message)
    })
  }

}
