import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/apiService';


@Component({
  selector: 'app-referencehistory',
  templateUrl: './referencehistory.page.html',
  styleUrls: ['./referencehistory.page.scss'],
})
export class ReferencehistoryPage implements OnInit {

  constructor(private apiService: ApiService, private _location: Location) { }

  ngOnInit() {
    this.getReferenceHistory()
  }

  onBack() {
    this._location.back();
  }
  listHistory: any = []
  getReferenceHistory() {
    this.apiService.getHistory().subscribe((res: any) => {
      console.log('error messgae ', res)
      this.listHistory = res.data
    }, error => {
      this.apiService.dengerMessage(error.message)
    })
  }
}
