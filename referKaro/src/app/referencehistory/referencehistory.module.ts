import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReferencehistoryPageRoutingModule } from './referencehistory-routing.module';

import { ReferencehistoryPage } from './referencehistory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReferencehistoryPageRoutingModule
  ],
  declarations: [ReferencehistoryPage]
})
export class ReferencehistoryPageModule {}
