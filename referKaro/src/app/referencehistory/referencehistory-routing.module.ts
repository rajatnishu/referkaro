import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReferencehistoryPage } from './referencehistory.page';

const routes: Routes = [
  {
    path: '',
    component: ReferencehistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReferencehistoryPageRoutingModule {}
