import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/apiService';

@Component({
  selector: 'app-referby',
  templateUrl: './referby.page.html',
  styleUrls: ['./referby.page.scss'],
})
export class ReferbyPage implements OnInit {

  constructor( private apiService  : ApiService, private _location:Location) { }

  ngOnInit() {
    this.referby()
  }

  onBack() {
    this._location.back();
  }

  referList : any =[]

  referby(){
    this.apiService.referBy().subscribe((res : any) =>{
      this.referList  = res.data
    },error =>{
      this.apiService.dengerMessage(error.message)
    })
  }

  

  action(value,data){
    let obj ={
      reference_id : value.reference_id,
      action : data
    }
    this.apiService.action(obj).subscribe((res : any) =>{
        this.referby()
    },error =>{
      this.apiService.dengerMessage(error.message)
    })
  }

}
