import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReferbyPage } from './referby.page';

const routes: Routes = [
  {
    path: '',
    component: ReferbyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReferbyPageRoutingModule {}
