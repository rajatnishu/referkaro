import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReferbyPageRoutingModule } from './referby-routing.module';

import { ReferbyPage } from './referby.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReferbyPageRoutingModule
  ],
  declarations: [ReferbyPage]
})
export class ReferbyPageModule {}
