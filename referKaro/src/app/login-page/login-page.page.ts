import { HttpClient } from '@angular/common/http'
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { environment } from '../../environments/environment';
import { ApiService } from '../service/apiService';
import { map } from 'rxjs/operators';
// import 'rxjs/Rx';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.page.html',
  styleUrls: ['./login-page.page.scss'],
})
export class LoginPagePage implements OnInit {

  constructor(private router: Router, private http: HttpClient, private ApiService: ApiService) { }

  ngOnInit() {

  }

  getNumber: any
  refferal_code: any = ''
  baseUrl = `${environment.apiUrl}`
  getOtp() {
    if (this.getNumber && this.getNumber.toString().length === 10) {
      let obj = {
        mobile: JSON.stringify(this.getNumber),
        refferal_code: this.refferal_code
      }
      this.ApiService.getLogIn(obj).subscribe((res: any) => {
        if (res.status === 1) {
          localStorage.setItem('mobile', obj.mobile)
          this.ApiService.successMessage("OTP sent succesfully!")
          this.router.navigate(['/', 'otp-page'])
        } else {
          this.ApiService.dengerMessage(res.message)
        }
      }, error => {
        this.ApiService.dengerMessage(error.message)
      })

    } else {
      this.ApiService.dengerMessage("Please enter a valid number!")
    }
  }
  term1: any = false;

  checkCondition(event,value) {
    if(value == "first"){
      if(event.detail.checked){
        this.term1 =true; 
      }
      else{
        this.term1= false;
      }
    }

  }

  gototerms(){
    debugger
    this.router.navigateByUrl('/conditions')
  }



}
