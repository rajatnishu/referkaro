import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import {  IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ModalpopupPageModule } from './modalpopup/modalpopup.module'
import { NgOtpInputModule } from  'ng-otp-input';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

// import {FCM} from '@ionic-native/fcm'
// import { AngularFirestoreModule } from '@angular/fire';
// import { AngularFirestore } from '@angular/fire/firestore';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, FormsModule, IonicModule.forRoot(), AppRoutingModule,HttpClientModule,ModalpopupPageModule, NgOtpInputModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
 