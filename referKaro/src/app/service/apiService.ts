
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ToastController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
// import 'rxjs/Rx';



@Injectable({
  providedIn: 'root'
})

export class ApiService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  baseUrl = `${environment.apiUrl}`
  constructor(private httpClient: HttpClient, public toastController: ToastController) { }



  async successMessage(msg) {
    const toast = await this.toastController.create({
      color: 'success',
      duration: 2000,
      message: msg,
    });

    await toast.present();
  }

  async dengerMessage(msg) {
    const toast = await this.toastController.create({
      color: 'danger',
      duration: 2000,
      message: msg,
    });

    await toast.present();
  }


  public getLogIn(item) : Observable<any> {
    return this.httpClient.post<any>(this.baseUrl + 'userLogin', item)
  
  }

  getLoginUser(obj){
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.get(this.baseUrl + 'v1/users/'+ obj, httpOptions )
  }

  updateUser(obj){
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
   return this.httpClient.post(this.baseUrl + 'v1/users/updateDetail', obj, httpOptions)
  }

  sendRefer(obj) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.post(this.baseUrl + 'v1/users/refereNow', obj, httpOptions)
  }

  otpVerifyApi(obj) {
    return this.httpClient.post(this.baseUrl + 'verifyOTP', obj, {})
  }
  allService() {
    return this.httpClient.post(this.baseUrl + 'allServices', '', {})
  }

  getHistory() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.post(this.baseUrl + 'v1/users/referenceHistory', '', httpOptions)
  }

  ownLoan() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.post(this.baseUrl + 'v1/users/availedServices', '', httpOptions)
  }

  referBy() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.post(this.baseUrl + 'v1/users/referBy', '', httpOptions)
  }

  action(obj) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.post(this.baseUrl + 'v1/users/referByAction', obj, httpOptions)
  }

  trasection() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })
    }
    return this.httpClient.post(this.baseUrl + 'v1/users/transactionsHistory', '', httpOptions)
  }

  getCondition(){
    return this.httpClient.post(this.baseUrl+ 'tc', '')
  }

  police(){
    return this.httpClient.post(this.baseUrl+ 'privacy', '')
  }
}
