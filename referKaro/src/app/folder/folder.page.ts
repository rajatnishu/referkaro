import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import { ApiService } from '../service/apiService';
import { ModalController } from '@ionic/angular';
import { ModalpopupPage } from '../modalpopup/modalpopup.page'
@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private ApiService: ApiService,private ModalController  : ModalController) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    this.getUser()
   
  }


  goToPage() {
    this.router.navigate(['/', 'wallet'])
  }


  userData : any = { } 
  getUser(){
    let id = localStorage.getItem('id')
    this.ApiService.getLoginUser(id).subscribe((res : any ) =>{
        this.userData = res.data
    }, error =>{
      this.ApiService.dengerMessage(error.error)
    })
  }

  mobileNumber: any

  OpenModal(){

    if (this.mobileNumber && this.mobileNumber.toString().length === 10) {
      this.ModalController.create({component : ModalpopupPage,componentProps: { value: this.mobileNumber }}).then((modalElement) =>{
        modalElement.present();
        this.mobileNumber =''
      })
     } else{
      this.ApiService.dengerMessage("Please enter a valid number!")
    }
  
  }

}
